<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'career-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
<?php $this->widget('ImperaviRedactorWidget', array(
    'selector' => '.redactor',
    'options' => array(
        'imageUpload'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'image')),
        'clipboardUploadUrl'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'clip')),
    ),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row-fluid">
	<div class="span8">
<div class="widget">
<h4 class="widgettitle">Data Career</h4>
<div class="widgetcontent">


	<?php echo $form->textFieldRow($model,'job_title',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'experience',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'work_location',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span5 redactor')); ?>

	<?php echo $form->textAreaRow($model,'requirement',array('rows'=>6, 'cols'=>50, 'class'=>'span5 redactor')); ?>

	<?php echo $form->dropDownListRow($model, 'status', array(
		'1'=>'Di Tampilkan',
		'0'=>'Di Sembunyikan',
	)); ?>
</div>
</div>
</div>
<div class="span4">
	<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Action</h4>
		    </div>
		    <div class="widgetcontent">
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>$model->isNewRecord ? 'Save And Add Item' : 'Save Item',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
				<?php if ($model->scenario == 'update'): ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('update', 'id'=>$model->id, 'type'=>'copy')),
					'label'=>'Copy',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
				<?php endif ?>
		    </div>
		</div>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
