<?php
$this->breadcrumbs=array(
	'People'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-book',
	'title'=>'People',
	'subtitle'=>'Data People',
);

$this->menu=array(
	array('label'=>'List People', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add People', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View People', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<div class="row-fluid">
	<div class="span8">
		<h1>Edit People</h1>
		<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
	</div>
	<div class="span4">
		<?php //$this->renderPartial('/setting/page_menu') ?>
	</div>
</div>
