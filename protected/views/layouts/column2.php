<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="subpage_outer">
	<?php echo $this->renderPartial('//layouts/_header', array()); ?>
	<?php echo $content ?>
</div>
<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
<?php $this->endContent(); ?>