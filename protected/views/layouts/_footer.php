
<footer class="foot">
	<div class="prelatife container"> 
		<div class="insides_footer">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<p class="t-copyrights">
						Copyright &copy; 2017 PT Berca Carrier Indonesia<br>
						Trademarks are proprietary to PT Berca Carrier Indonesia
					</p>
				</div>
				<div class="col-md-6 col-sm-6 text-right">
					<div class="socmed_footer">
						<?php /*
						<?php if ($this->setting['url_twitter'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_twitter'] ?>"><i class="fa fa-twitter"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($this->setting['url_instagram'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>"><i class="fa fa-instagram"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($this->setting['url_facebook'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_facebook'] ?>"><i class="fa fa-facebook-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php endif ?>
						*/ ?>
						<?php if ($this->setting['url_linkedin'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_linkedin'] ?>"><i class="fa fa-linkedin"></i></a>
						<?php endif ?>
						<?php /*
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php if ($this->setting['url_youtube'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_youtube'] ?>"><i class="fa fa-youtube"></i></a>
						<?php endif ?>
						*/ ?>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</footer>