<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<!-- Start fcs -->
<?php
$criteria=new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->group = 't.id';
$criteria->order = 't.id ASC';
$slide = Slide::model()->with(array('description'))->findAll($criteria);
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife">
    <div id="myCarousel_home" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php foreach ($slide as $key => $value): ?>
                <li data-target="#myCarousel_home" data-slide-to="<?php echo $key; ?>" <?php if($key == 0){ ?>class="active"><?php } ?></li>
            <?php endforeach; ?>
        </ol>
        <div class="carousel-inner">
            <?php foreach($slide as $key => $value): ?>
            <div class="carousel-item <?php if ($key == 0): ?>active<?php endif; ?>">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900,883, '/images/slide/'.$value->image, array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive full_img">
            </div>
            <?php endforeach ?>
        </div>
    </div>
    <div class="backs_bottoms_tn_fcs">
        <div class="inside">
            <div class="txts">
                <p><?php echo $this->setting['slide_title'] ?></p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!-- End fcs -->

<?php echo $content ?>

<?php  echo $this->renderPartial('//layouts/_footer', array()); ?>

<script type="text/javascript">
    var $item = $('.carousel .carousel-item'); 
    var $wHeight = $(window).height();
    $item.eq(0).addClass('active');
    $item.height($wHeight); 
    $item.addClass('full-screen');

    $('.carousel .carousel-item img').each(function() {
      var $src = $(this).attr('src');
      var $color = $(this).attr('data-color');
      $(this).parent().css({
        'background-image' : 'url(' + $src + ')',
        'background-color' : $color
      });
      $(this).remove();
    });

    $(window).on('resize', function (){
      $wHeight = $(window).height();
      $item.height($wHeight);
    });

    $('.carousel').carousel({
      interval: 4000,
      pause: "false"
    });
</script>
<?php $this->endContent(); ?>