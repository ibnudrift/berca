<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;

    $criteria = new CDbCriteria;
    $criteria->with = array('description');
    $criteria->addCondition('parent_id = 0');
    $criteria->addCondition('type = "category"');
    $criteria->addCondition('description.language_id = :language_id');
    $criteria->params[':language_id'] = $this->languageID;
    $criteria->order = 'sort ASC';
    $dataCategory = PrdCategory::model()->findAll($criteria);
?>
<div class="outers_back-header">
<header class="head">

  <div class="d-none d-sm-block">
    <div class="prelatife container_header">
      <div class="ins">
        <div class="row">
          <div class="col-md-6">
            <div class="lgo_web_header">
              <a class="d-block" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img src="<?php echo $this->assetBaseurl; ?>lgo-header-berca.png" alt="" class="img img-fluid d-inline-block align-middle padding-right-15">
                <img src="<?php echo $this->assetBaseurl; ?>lgo-headers_carrier.png" alt="" class="img img-fluid d-inline-block align-middle padding-right-30 lgo2">
                <span class="d-inline-block align-middle">Welcome to PT. Berca Carrier Indonesia</span>
              </a>
              <div class="clear"></div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="rights_header text-right">
              <div class="tops">
                <div class="brands_lgo_headers">
                  <a target="_blank" href="http://www.carrier.co.id" class="blue">CARRIER</a>
                  &nbsp;&nbsp;&nbsp;
                  <a target="_blank" href="http://www.toshiba-ac.co.id/" class="red">TOSHIBA - AC</a>
                </div>
                <div class="box_search_hdr">
                  <form action="<?php echo CHtml::normalizeUrl(array('/home/news')); ?>" method="get" class="form-inline">
                    <div class="form-group">
                      <input type="text" class="form-control" name="q" value="<?php echo $_GET['q'] ?>" placeholder="Search">
                    </div>
                    <button class="btn btn-link"><i class="fa fa-search"></i></button>
                  </form>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>
              </div>

              <div class="clear clearfix"></div>

              <div class="bottoms">
                <div class="top-menu d-block">
                  <ul class="list-inline nr-0 m-0">
                    <li class="list-inline-item <?php echo ($active_menu_pg == 'home/index' || $active_menu_pg == '')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
                    <li class="list-inline-item <?php echo ($active_menu_pg == 'home/about')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">OUR JOURNEY</a></li>
                    <li class="list-inline-item <?php echo ($active_menu_pg == 'home/news')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/news')); ?>">NEWS</a></li>
                    <li class="list-inline-item <?php echo ($active_menu_pg == 'home/career')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/career')); ?>">CAREER</a></li>
                    <li class="list-inline-item <?php echo ($active_menu_pg == 'home/contact')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
                  </ul>
                </div>
                <?php /*
                <div class="bxn_right_language d-inline">
                  &nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="#" class="active">EN</a>
                  &nbsp;&nbsp;
                  <a href="#">ID</a>
                </div>
                */ ?>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end bottom -->
    
  <div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
            <img src="<?php echo $this->assetBaseurl; ?>lgo-header-berca.png" alt="" class="img img-fluid">
          </a>
          
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="<?php echo ($active_menu_pg == 'home/index' || $active_menu_pg == '')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
            <li class="<?php echo ($active_menu_pg == 'home/about')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">OUR JOURNEY</a></li>
            <li class="<?php echo ($active_menu_pg == 'home/news')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/news')); ?>">NEWS</a></li>
            <li class="<?php echo ($active_menu_pg == 'home/career')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/career')); ?>">CAREER</a></li>
            <li class="<?php echo ($active_menu_pg == 'home/contact')? 'active':''; ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
          </ul>
          <div class="clear height-10"></div>
           <div class="brands_lgo_headers">
              <a target="_blank" href="http://www.carrier.co.id" class="blue">CARRIER</a>
              &nbsp;&nbsp;&nbsp;
              <a target="_blank" href="http://www.toshiba-ac.co.id/" class="red">TOSHIBA - AC</a>
            </div>
            <?php /*
          <div class="d-inline bxn_right_language">
             <a href="#" class="active">EN</a>
              &nbsp;&nbsp;|&nbsp;&nbsp;
              <a href="#">IN</a>
          </div>
          */ ?>
          <div class="clear"></div>
        </div>
    </nav>
    <div class="clear"></div>
  </div>
  <!-- end bottom responsive -->

  <div class="clear"></div>
</header>
</div>
<?php /*
<script type="text/javascript">
  $(document).ready(function(){
      $('.nl_popup a').live('hover', function(){
          $('.popup_carts_header').fadeIn();
      });
      $('.popup_carts_header').live('mouseleave', function(){
        setTimeout(function(){ 
            $('.popup_carts_header').fadeOut();
        }, 500);
      });
  });
</script>

<script type="text/javascript">
    $(function(){
      $('#myAffix').affix({
        offset: {
          top: 300
        }
      })
    })
  </script>

<section id="myAffix" class="header-affixs affix-top">
  <div class="clear height-5"></div>
  <div class="prelatife container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <div class="lgo-web-web_affix">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
            <img src="<?php echo $this->assetBaseurl ?>lgo_stumble_wt_txt.png" alt="" class="img-responsive d-inline" style="max-width: 75px;">
            STUMBLE UPON
          </a>
        </div>
      </div>
      <div class="col-md-9 col-sm-9">
        <div class="text-right"> 
          <div class="clear height-20"></div>
          <div class="menu-taffix">
            <ul class="list-inline">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">BUY COFFEE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/howto')); ?>">HOW TO ORDER</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/wholesale')); ?>">WHOLESALE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT US</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">EVENTS</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>
*/ ?>