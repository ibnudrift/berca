
<footer class="foot">
	<div class="prelatife container"> 
		<div class="insides_footer">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<p class="t-copyrights">
						Copyright &copy; 2017 Berca Carrier Indonesia. Website design by <a href="http://www.markdesign.net/" target="_blank" title="Website Design Surabaya">Mark Design.</a>
					</p>
				</div>
				<div class="col-md-6 col-sm-6 text-right">
					<div class="socmed_footer">
						<a href="#"><i class="fa fa-twitter"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="#"><i class="fa fa-instagram"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="#"><i class="fa fa-facebook-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="#"><i class="fa fa-linkedin"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="#"><i class="fa fa-youtube"></i></a>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</footer>