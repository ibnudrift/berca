<div class="blocks_subpage_banner career mah546">
  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900, 550, '/images/static/'.$this->setting['illustration_career_pict'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block full_pict">
  <div class="insides text-center">
    <h3 class="sub_title_p"><?php echo $this->setting['career_title'] ?></h3>
    <div class="clear"></div>
  </div>
</div>
<style type="text/css">
  img.full_pict{
    width: 100%;
  }
  .outers_back-header{
    position: relative;
  }
  .blocks_subpage_banner.career{
    height: auto; min-height: inherit;
  }
  .blocks_subpage_banner.career .insides{
    position: absolute; z-index: 50;
    left: 0px; width: 100%;
    top: 50%; height: 100%;
  }
  .blocks_subpage_banner h3.sub_title_p{
    padding-top: 0;
  }
</style>
<div class="clear"></div>
  <div class="subpage static_about">
  <div class="prelatife container">
    <div class="tops_contStatic">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <h2 class="titles"><?php echo $this->setting['career_title'] ?></h2>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="outs_breadcrumb text-right float-right">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li class="active"><?php echo $this->setting['career_title'] ?></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="prelatife container">
    <div class="clear height-25"></div>
    <div class="content-text middles_contbottom text-center pg_career">
      <div class="row">
        <div class="col-md-12">
          <?php echo $this->setting['career_content'] ?>

          <div class="clear height-40"></div>
          <div class="blocks_carrer_post">
            <div class="top">
              <h5 class="text-center">AVAILABLE POSITIONS</h5>
            </div>
            <div class="clear height-25"></div>

            <div class="data_middles">
              <div class="tops_d">
                <div class="row default">
                  <div class="col-md-8 col-sm-6">
                    <b>TITLE</b>
                  </div>
                  <div class="col-md-2 col-sm-3 hidden-xs">
                    <b>MIN. EXPERIENCE</b>
                  </div>
                  <div class="col-md-2 col-sm-3  hidden-xs">
                    <b>WORK LOCATION</b>
                  </div>
                </div>
              </div>
              <div class="panel-group boxeds_list_c_panel" id="accordion" role="tablist" aria-multiselectable="true">
<?php
$careers = Career::model()->findAll();
?>
                <?php foreach ($careers as $key => $value): ?>
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading_<?php echo $key ?>">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $key ?>" aria-expanded="true" aria-controls="collapse_<?php echo $key ?>">
                        <div class="row default">
                          <div class="col-md-8 col-sm-6">
                            <?php echo $value->job_title ?>
                          </div>
                          <div class="col-md-2 col-sm-3">
                            <?php echo $value->experience ?>
                          </div>
                          <div class="col-md-2 col-sm-3">
                            <?php echo $value->work_location ?>
                          </div>
                        </div>
                      </a>
                    </h4>
                  </div>
                  <div id="collapse_<?php echo $key ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?php echo $key ?>">
                    <div class="panel-body">
                     <div class="row">
                       <div class="col-md-6 col-sm-6">
                        <div class="padding-right-30">
                         <p><b>JOB DESCRIPTION</b></p>
                         <?php echo $value->description ?>
                         </div>
                       </div>
                       <div class="col-md-6 col-sm-6">
                         <p><b>REQUIREMENT</b></p>
                          <?php echo $value->requirement ?>
                       </div>
                     </div>
                    </div>
                  </div>
                </div>
                <?php endforeach ?>
              </div>
              <div class="clear"></div>
            </div>
            <!-- End group list carrer job -->

            <div class="clear"></div>
          </div>

          <div class="clear"></div>
        </div>
      </div>

      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
  <!-- end container -->
