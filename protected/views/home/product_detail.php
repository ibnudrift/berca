<div class="subpage_product">
  <section class="default_sc blocks_bannerBox_home back-whiteimp" id="block_homesection">
    <div class="clear height-25"></div>

    <div class="in_box_product back-white">
      <div class="prelatife container">

        <div class="tops purple text-center">
          <div class="row">
            <div class="col-md-6">
              <h3 class="sub_title">Kategori Audio Video</h3>
            </div>
            <div class="col-md-6">
              <div class="t_back_categorys">
                <a href="#">Kembali ke Kategori Audio Video &nbsp;<i class="fa fa-chevron-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="insides padding-top-15 middles_inProduct_detail">
          
          <!-- Start Box Products Detail -->
          <div class="box_top_detail_product">
            <div class="row row-eq-height">
              <div class="col-md-9">
              <div class="padding_leftxs">
                  <div class="lg_brand padding-bottom-10"><img src="<?php echo $this->assetBaseurl ?>ics_samsung-gl.png" alt="" class="img-responsive"></div>
                  <h1 class="titles">Samsung 65" 4K LED Ultra-HD Curved Smart TV with 2-Year Warranty</h1>
                  <div class="clear height-15"></div>
                  <div class="row prelatife">
                    <div class="col-md-7">
                      <div class="picture"><img src="<?php echo $this->assetBaseurl ?>ex_big_product.jpg" alt="" class="img-responsive"></div>
                    </div>
                    <div class="col-md-5 h100per">
                      <div class="fright mw285 padding-top-20 info_benefit">
                        <p><b>KEUNGGULAN PRODUK</b><br>
                            Connects to iPad Pro with the Smart
                            Connector. Free from switches, plugs and
                            even pairing. No batteries or charging
                            required.</p>

                        <div class="clear"></div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="shares_box">
                            <span class="d-inline padding-right-10 tn">Share this:</span> <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58f5e4af3ee8fe7e"></script> 
                            <div class="clear"></div>
                            <div class="addthis_inline_share_toolbox"></div>
                          </div>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="rights_box_blue_prdDetail">
                  <div class="b_img_sales">
                    <img src="<?php echo $this->assetBaseurl; ?>backs_con_right_top_products2-big.png" alt="">
                  </div>
                  <div class="pn_price paddings">
                    <span class="top_price">Harga Normal RP <i>25,000,000,-</i></span>
                    <span>Kini RP <b>22,000,000,-</b></span>
                    <span class="prc_bottom">Hemat RP 3,000,000,-</span>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                  <div class="blacks_info paddings mb1">
                    <div class="d-inline fleft">
                      <img src="<?php echo $this->assetBaseurl; ?>bc_icon_stars.png" alt="">
                    </div>
                    <div class="d-inline fleft mw255">
                      <p><b>FREE DELIVERY</b><br>
                        Untuk pembelian di Surabaya, Jakarta, Malang,
                        Sidoarjo, Balikpapan, Makassar.</p>
                    </div> <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                  <div class="blacks_info paddings">
                    <div class="d-inline fleft">
                      <img src="<?php echo $this->assetBaseurl; ?>bc_icon_stars.png" alt="">
                    </div>
                    <div class="d-inline fleft mw255">
                      <p><b>PICK UP AT STORE</b><br>
                      Pengambilan di hari yang sama dapat dilakukan di cabang UFO terdekat jika stok barang terdapat di lokasi terdekat anda.</p>
                    </div> <div class="clear"></div>
                  </div>
                  <div class="clear height-25"></div>
                  <div class="paddings">
                    <form class="form-inline" method="post" action="#">
                      <div class="form-group">
                        <label for="">Jumlah</label>
                        <input type="number" class="form-control" value="1">
                      </div>
                      <button class="btn btn-default btns_submits_red"></button>
                    </form>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="clear height-10"></div><div class="height-3"></div>
          <div class="bottoms_prdDetail row">
            <div class="col-md-9">
            <div class="box_description_bottomLeft">
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">DESKRIPSI</a></li>
                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">SPESIFIKASI</a></li>
                </ul>

                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="home">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nulla urna, congue sit amet augue vitae, rhoncus egestas ipsum. Fusce commodo egestas lorem sed suscipit. Mauris molestie tincidunt ante non mattis. Vivamus blandit metus at nisi feugiat euismod. In sit amet laoreet libero. Maecenas ac pretium quam, in finibus leo. Duis tincidunt, urna eget ornare dictum, ligula quam bibendum enim, sit amet venenatis odio sapien id nibh.</p>
                    <ul>
                      <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </li>
                      <li>Iure repellat, ullam expedita quasi dicta. </li>
                      <li>Laudantium autem amet alias provident, voluptas laborum natus vitae, placeat mollitia accusamus ad vero dicta aspernatur!</li>
                    </ul>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="profile">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolorum, nesciunt laborum similique, modi iste cum porro odit culpa nemo doloribus facilis officiis, dicta, temporibus molestias blanditiis quasi! Necessitatibus, assumenda.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolorum, nesciunt laborum similique, modi iste cum porro odit culpa nemo doloribus facilis officiis, dicta, temporibus molestias blanditiis quasi! Necessitatibus, assumenda.</p>
                  </div>
                </div>

              </div>
              <!-- end contents tab -->
            </div>
            <div class="col-md-3">
              <div class="boxs_wrp_othersProducts">
                <div class="tops"><span>PRODUK LAIN UNTUK ANDA</span></div>
                <div class="middles">
                  <div class="lists_others_product">
                    <?php for ($i=0; $i < 4; $i++) { ?>
                    <div class="items">
                      <div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>other-products.jpg" alt="" class="img-responsive"></a></div>
                      <div class="info">
                        <div class="titles"><a href="#">Samsung 65" 4K LED Ultra-HD Curved Smart TV with 2-Year Warranty</a></div>
                        <div class="prices">
                          <span class="lt">RP <i>25,000,000,-</i></span>
                          <div class="clear"></div>
                          <span>RP 22,000,000,-</span>
                        </div>
                        <div class="clear"></div>
                      </div>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                  </div>
                  <div class="celar"></div>
                </div>
                <div class="clear"></div>
              </div>

            </div>
          </div>
          <!-- End Box Products Detail -->

          <div class="clear height-20"></div>

          <div class="clear height-25"></div>
          <div class="clear"></div>
        </div>
        <!-- end insides -->
      </div>
    </div>
    <!-- End sub kategori -->

  </section>
  <div class="clear"></div>
</div>