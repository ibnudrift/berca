<section class="default_sc" id="block_homesection1" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900, 550, '/images/static/'.$this->setting['journey_picture'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
	<div class="insides">
		<img src="<?php echo $this->assetBaseurl ?>txt-our_journey-berca.png" alt="" class="img-responsive center-block">
		<p><?php echo nl2br($this->setting['journey_title']) ?></p>
		<a href="<?php echo $this->setting['journey_button_url'] ?>" class="btn btn-link btns_set_default"><?php echo $this->setting['journey_button_text'] ?></a>
		<div class="clear"></div>
	</div>
</section>

<section class="default_sc" id="block_homesect2">
	<div class="prelatife container_w2">
		<div class="insides">
			<div class="tops padding-bottom-45">
				<h3 class="s_title"><?php echo $this->setting['banner_title'] ?></h3>
				<p><?php echo $this->setting['banner_subtitle'] ?></p>
			</div>
			<div class="lists_banner_default animations">
				<div class="row default">
					<div class="col-md-6 col-sm-6">
						<div class="items">
							<a href="http://www.carrier.co.id" target="_blank">
						        <div class="work-img"><img src="<?php echo $this->assetBaseurl ?>pict-berca-11-n.jpg" alt="Carrier">
						        </div>
						        <div class="work-intro">
						            <h3 class="work-title" style="text-align:center"><img src="<?php echo $this->assetBaseurl ?>lgo_carrier-bg_bottom.png" alt="Carrier"></h3>
						        </div>
						    </a>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="items toshiba">
							<a href="http://www.toshiba-ac.co.id/" target="_blank">
						        <div class="work-img"><img src="<?php echo $this->assetBaseurl ?>pict-berca-12-n.jpg" alt="Toshiba">
						        </div>
						        <div class="work-intro">
						            <h3 class="work-title" style="text-align:center"><img src="<?php echo $this->assetBaseurl ?>lgo_toshiba-bg_bottom.png" alt="Toshiba"></h3>
						        </div>
						    </a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="default_sc" id="blocks_section_homeNews">
	<div class="tops_news">
		<h3 class="sub_title">Corporate News</h3>
	</div>
	<div class="prelatife container_w2">
		<div class="insides"> <div class="clear height-5"></div>
<?php
		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->order = 'date_input DESC';
		$criteria->limit = 3;
		$blogHead = Blog::model()->findAll($criteria);

?>
			<div class="lists_dt_news_default">
				<div class="row default">
					<?php foreach ($blogHead as $key => $value): ?>
					<div class="col-md-4 col-sm-6">
						<div class="items">
							<div class="pict"><a href="<?php echo CHtml::normalizeUrl(array('/home/newsDetail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(326,149, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a></div>
							<div class="info">
								<a href="<?php echo CHtml::normalizeUrl(array('/home/newsDetail', 'id'=>$value->id)); ?>"><h4 class="titles"><?php echo $value->description->title ?></h4></a>
								<span class="dates"><?php echo date('d M Y', strtotime($value->date_input)) ?></span>
								<p><?php echo substr(strip_tags($value->description->content), 0, 200) ?>....</p>
								<a href="<?php echo CHtml::normalizeUrl(array('/home/newsDetail', 'id'=>$value->id)); ?>" class="btn btn link btns_more_news">Read More</a>
							</div>
						</div>
					</div>
					<?php endforeach ?>
				</div>
			</div>
			<!-- End list news default -->
			<div class="clear height-50"></div>
			<div class="text-center vws_all_news_bottom">
				<a href="<?php echo CHtml::normalizeUrl(array('/home/news')); ?>" class="btn btn-link">CLICK HERE TO VIEW ALL NEWS</a>
			</div>

			<div class="clear"></div>
		</div>
	</div>
</section>