<!-- Start fcs -->
<div class="fcs-wrapper outers_fcs_wrapper prelatife">
    <div id="myCarousel_home" class="carousel fade" data-ride="carousel">
        <?php
        $criteria=new CDbCriteria;
        $criteria->with = array('description');
        $criteria->addCondition('description.language_id = :language_id');
        $criteria->params[':language_id'] = $this->languageID;
        $criteria->group = 't.id';
        $criteria->order = 't.id ASC';
        $slide = Slide::model()->with(array('description'))->findAll($criteria);
        ?>
        <ol class="carousel-indicators">
            <?php foreach ($slide as $key => $value): ?>
                <li data-target="#myCarousel_home" data-slide-to="<?php echo $key; ?>" <?php if($key == 0){ ?>class="active"><?php } ?></li>
            <?php endforeach; ?>
        </ol>
        <div class="carousel-inner">
            <?php foreach ($slide as $key => $value): ?>
            <div class="item active<?php //if ($key == 0): ?><?php //endif ?>">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1600,583, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                <?php /*<div class="carousel-caption">
                	<div class="bxsl_tx_fcs">
                		<h3><?php echo $value->description->title ?></h3>
                		<span><?php echo $value->description->subtitle ?></span>
                		<p><?php echo nl2br($value->description->content) ?></p>
                		<div class="clear height-5"></div>
                        <?php if ($value->description->url != ''): ?>
                		<a href="<?php echo $value->description->url ?>" class="btn btn-default btn_brown_def"><?php echo $value->description->url_teks ?></a>
                        <?php endif ?>
                		<div class="clear"></div>
                	</div>
                </div>*/ ?>
            </div>
            <?php endforeach ?>
        </div>

    </div>
    <div class="clear"></div>
</div>
<!-- End fcs -->

<!-- conts box top -->
<section class="default_sc blocks_bannerBox_home" id="block_homesection">
	<div class="in_box_product back-white">
		<div class="tops blue text-center">
			<h3 class="sub_title">Kategori Audio Video</h3>
		</div>
		<div class="prelatife container">
			<div class="insides padding-top-15">

				<ul class="list-inline text-center lists_subCategory_data">
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Televisions</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Home Theatre</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Players & Recorders</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Wireless Audio</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Special Items</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Wireless Audio</a></div>
						</div>
					</li>
					<li>
						<div class="items">
							<div class="pict"><a href="#"><img src="<?php echo $this->assetBaseurl ?>ex_products_sub.jpg" alt="" class="img-responsive"></a></div>
							<div class="clear"></div>
							<div class="names"><a href="#">Special Items</a></div>
						</div>
					</li>
				</ul>

				<div class="clear height-25"></div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!-- End sub kategori -->

	<div class="in_box_product back-white">
		<div class="tops yellow text-center">
			<h3 class="sub_title">Penawaran Istimewa & Terbatas di Audio Video!</h3>
		</div>
		<div class="prelatife container">
			<div class="insides padding-top-15">
				<div class="lists_product_data">
					<?php for ($i=0; $i < 16; $i++) { ?>
						<div class="items prelatife">
							<div class="t_info prelatife">
								<div class="row">
									<div class="col-xs-4 lgo_brands">
										<img src="<?php echo $this->assetBaseurl ?>ics_samsung-gl.png" alt="">
									</div>
									<div class="col-xs-8">
										<div class="blocks_recom">
											<?php if ($i == 2): ?>
											<img src="<?php echo $this->assetBaseurl ?>backs_con_right_top_products-yellow2.png" alt="">	
											<?php else: ?>
											<img src="<?php echo $this->assetBaseurl ?>backs_con_right_top_products2.png" alt="">
											<?php endif ?>
										</div>
									</div>
								</div>
							</div>
							<div class="b_titles">
								Samsung 65&quot; 4K LED Ultra-HD Curved Smart TV with 2-Year Warranty
							</div>
							<div class="picture">
								<img src="<?php echo $this->assetBaseurl ?>ex-products.jpg" alt="" class="img-responsive center-block">
							</div>
							<div class="bloc_bottom">
								<div class="row">
									<div class="col-xs-9">
										<div class="prices">
											<span class="through">Harga Normal RP <i>25,000,000,-</i></span>
											<span>Kini <b>RP 22,000,000,-</b></span>
											<small>Hemat RP 3,000,000,-</small>
										</div>
									</div>
									<div class="col-xs-3 text-right">
										<i class="infree_shipping"></i>
										<div class="clear"></div>
										<a href="#" class="btn btn-link btns_blueProducts"><i class="fa fa-search"></i></a>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					<?php } ?>
				</div>
				<!-- end list product data -->
				<div class="text-center bs_bottom_pagin">
					<p>Halaman 1 dari 3</p>
				</div>
				<div class="clear height-50"></div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

</section>
