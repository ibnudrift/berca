<div class="blocks_subpage_banner contact mah546" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900, 550, '/images/static/'.$this->setting['illustration_contact_pict'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
  <div class="insides text-center">
    <h3 class="sub_title_p"><?php echo $this->setting['contact_title'] ?></h3>
    <div class="clear"></div>
  </div>
</div>

<div class="clear"></div>
  <div class="subpage static_about">
  <div class="prelatife container">
    <div class="tops_contStatic">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <h2 class="titles"><?php echo $this->setting['contact_title'] ?></h2>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="outs_breadcrumb text-right float-right">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li class="active"><?php echo $this->setting['contact_title'] ?></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="prelatife container">
    <div class="clear height-25"></div>
    <div class="content-text middles_contbottom text-left pg_career">
      <div class="row">
        <div class="col-md-12">
          <?php echo $this->setting['contact_content'] ?>
          <div class="clear height-25"></div>

          <div class="row default blocks_contact_pg">
            <div class="col-md-3">
              <div class="lefts_c">
                <p>Phone.<br />
                <a class="vw_phone" href="tel:<?php echo $this->setting['contact_phone'] ?>"><?php echo $this->setting['contact_phone'] ?></a></p>

                <p class="hide">Email.<br />
                <a class="vw_phone" href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></p>

                <p>Headquarter.<br />
                <?php echo nl2br($this->setting['contact_address']) ?>
                <br>
                <a href="<?php echo $this->setting['contact_map'] ?>" target="_blank">View on google map</a></p>
                <div class="clear"></div>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_c">
                <?php echo $this->renderPartial('//home/_form_contact2', array('model' => $model)); ?>
                <div class="clear"></div>
              </div>
            </div>
          </div>
          <!-- End contact insides dn -->

        </div>
      </div>

      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
  <!-- end container -->
