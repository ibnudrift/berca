<div class="blocks_subpage_banner about mah546">
  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900, 550, '/images/static/'.$this->setting['illustration_about_pict'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid mx-auto full_pict">
  <div class="insides text-center">
    <h3 class="sub_title_p"><img src="<?php echo $this->assetBaseurl ?>txt-our_journey-berca.png" alt=""></h3>
    <div class="clear"></div>
  </div>
</div>
<style type="text/css">
  img.full_pict{
    width: 100%;
  }
  .outers_back-header{
    position: relative;
  }
  .blocks_subpage_banner.about{
    height: auto; min-height: inherit;
  }
  .blocks_subpage_banner.about .insides{
    position: absolute; z-index: 50;
    left: 0px; width: 100%;
    top: 50%; height: 100%;
  }
  .blocks_subpage_banner h3.sub_title_p{
    padding-top: 0;
  }
</style>

<div class="clear"></div>

<div class="subpage static_about">
  <div class="prelatife container">
    <div class="tops_contStatic">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <h2 class="titles"><?php echo $this->setting['about_titles'] ?></h2>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="outs_breadcrumb text-right float-right">
            <ol class="breadcrumb text-right m-0">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li class="active"><?php echo $this->setting['about_titles'] ?></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="prelatife container">
    <div class="clear height-25"></div>
    <div class="content-text middles_contbottom">
      <div class="row">
        <div class="col-md-6">
          <div class="lefts_cont">
            <?php echo $this->setting['about_content'] ?>
          </div>
        </div>
        <div class="col-md-6">
          <div class="picts_about">
            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(636, 453, '/images/static/'.$this->setting['about_picture'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
          </div>
        </div>
      </div>

      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
  <div class="clear"></div>

  <div class="blocks_middle_creamAbout prelatife">
    <div class="prelatife container">
      <div class="insides content-text">
        <div class="clear height-40"></div>

          <div class="lists_middlesn_cream">
            <div class="lists">
              <div class="row row-eq-height">
                <div class="col-lg-4 col-md-4">
                  <div class="picts"><img src="<?php echo $this->assetBaseurl ?>bck_puzzle-3.png" alt="" class="img img-fluid mx-auto"></div>
                </div>
                <div class="col-lg-8 col-md-8">
                  <div class="desc pt_75">
                    <?php echo $this->setting['about_mission_content'] ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="lists">
              <div class="row row-eq-height">
                <div class="col-md-4 col-md-4">
                  <div class="picts"><img src="<?php echo $this->assetBaseurl ?>bck_puzzle-1.png" alt="" class="img img-fluid mx-auto"></div>
                </div>
                <div class="col-md-8 col-md-8">
                  <div class="desc pt_75">
                    <?php echo $this->setting['about_purpose_content'] ?>
                  </div>
                </div>
              </div>
            </div>

            <?php /*
            <div class="lists no_border d-none">
              <div class="row row-eq-height">
                <div class="col-md-4">
                  <div class="picts"><img src="<?php echo $this->assetBaseurl ?>bck_puzzle-3.png" alt="" class="img img-fluid mx-auto"></div>
                </div>
                <div class="col-md-8">

                  <div class="desc pt_75">
                    <div class="subs_lists_item">
                      <div class="row">
                        <?php for ($i=1; $i < 5; $i++) { ?>
                        <div class="col-md-3 col-sm-6">
                          <div class="lists">
                            <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(636, 453, '/images/static/'.$this->setting['about_vision_icon'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid mx-auto"></div>
                            <div class="desc">
                              <?php echo $this->setting['about_vision_text'.$i] ?>
                            </div>
                          </div>
                        </div>
                        <?php } ?>
                      </div>
                    </div>

                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
            */ ?>

          </div>

        <div class="clear height-50"></div>
        <div class="clear height-40"></div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- End block creamAbout -->

  <?php /*
  <div class="blocks_middle_content history_block prelatife">
    <div class="container content-text">
    <div class="row row-eq-height">
      <div class="col-md-6 back-grey">
        <div class="inside maw475">
          <h3>Berca&rsquo;s history time line</h3>

          <p>We are the leading company for food waste disposal based in Korea, proud to introduce to you, SmartCara our revolutionary product for food waste disposal.</p>

          <p>As we move into a more environmentally conscious era which requires us to be socially responsible towards the rest of society and future generations. the act of disposing waste in an effective manner that requires minimum waste land is of the utmost importance. after ten years of research and development, we have invented the perfect solution, a food waste disposal unit which would make the world a healthier and better place to live in for now and the next generation.</p>

          <div class="clear"></div>
        </div>
      </div>
      <div class="col-md-6 back-cream">
        <div class="inside timesline_blockhistory">
          <ul class="list-unstyled">
            <li class="dropdown">
              <a href="javascript:;"><span class="year">1993</span> <i class="lines_1"></i> <span class="month">APR</span> <div class="info">First partnership with Carrier</div></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">MAY</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a></li>
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">AUG</span> <div class="info">Carrier to introduce the HVAC solution to the public</div></a></li>
              </ul>
            </li>
            <li>
              <a href="javascript:;"><span class="year">1998</span> <i class="lines_1"></i> <span class="month">JAN</span> <div class="info">4 Network offices opening on Java, covering Surabaya, Semarang, Yogyakarta, Bandung and Bali.</div></a>
            </li>
            <li class="dropdown">
              <a href="javascript:;"><span class="year">2005</span> <i class="lines_1"></i> <span class="month">MAY</span> <div class="info">Carrier launched  AquaEdge&reg; 23XRV with Greenspeed Technology</div></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">MAY</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a></li>
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">AUG</span> <div class="info">Carrier to introduce the HVAC solution to the public</div></a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="javascript:;"><span class="year">2008</span> <i class="lines_1"></i> <span class="month">JAN</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">APR</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a></li>
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">SEP</span> <div class="info">Carrier to introduce the HVAC solution to the public</div></a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="javascript:;"><span class="year">2017</span> <i class="lines_1"></i> <span class="month">JAN</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">FEB</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a></li>
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">APR</span> <div class="info">Carrier to introduce the HVAC solution to the public</div></a></li>
              </ul>
            </li>
          </ul>
          <div class="clear"></div>
        </div>
      </div>
    </div>
    </div>
    <div class="clear"></div>
  </div>

  <div class="block_about_vision">
    <div class="prelatife container">
      <div class="clear height-50"></div>
      <div class="clear height-50"></div>
      <div class="clear height-40"></div>

      <div class="cont_vision text-center">
        <div class="row default">
          <div class="col-md-6 col-sm-6 bdr_right">
            <div class="texts">
              <h5>OUR VISION</h5>
              <div class="clear"></div>
              <p>Delivering powerful solutions to both retail and corporate market through world&#39;s best brand and product, and thus became the standard for the needs of high-performance and reliable air cooling / refrigeration in Indonesia.</p>
              <div class="clear"></div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="texts">
              <h5>OUR MISSION</h5>
              <div class="clear"></div>
              <p>Introducing a broad range of applications in air conditioning and refrigeration technology, backed with full support of dedicated field professionals and product quality assurance.</p>
              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="clear"></div>
    </div>
  </div>
  */ ?>

  <section class="blocks_managements_about_outer">
    <div class="outer_block_managements" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900, 653, '/images/static/'.$this->setting['illustration_management_pict'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>'); background-position: top right;">
      <div class="prelatife container">
        <div class="insides content-text">
          <div class="clear height-50"></div>
          <div class="height-10"></div>
          <div class="row">
            <div class="col-md-6">
              <div class="lefts_contn">
                <h4><?php echo $this->setting['about_management_title'] ?></h4>
                <div class="clear"></div>
                <?php echo $this->setting['about_management_content'] ?>
                <div class="clear"></div>
              </div>

            </div>
            <div class="col-md-6">
              <div class="d-block d-sm-none">
                <div class="views_back_olnd">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900, 653, '/images/static/'.$this->setting['illustration_management_pict'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>

    <!-- list key people -->
    <div class="outers_block_keyPeople"> 
      <div class="prelatife container">

        <div class="ins_middle_keyPeople prelatife">
          <h4>Key People</h4>
          <div class="clear"></div>

          <div class="lists_defaults_keyPeople">
            <div class="row default">
            <?php
            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('active = "1"');
            $criteria->addCondition('description.language_id = :language_id');
            $criteria->params[':language_id'] = $this->languageID;
            $criteria->order = 'sorting ASC';
            $peoples = People::model()->findAll($criteria);
            ?>
            <?php foreach ($peoples as $key => $value): ?>
              <div class="col-sm-4 col-md-25 col-6">
                <div class="items">
                  <div class="top padding-bottom-20">
                    <div class="picture fleft"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(114,141, '/images/people/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></div>
                    <p><b><?php echo $value->description->title ?></b><br><?php echo $value->description->position ?></p>
                    <div class="clear"></div>
                  </div>
                  <?php echo $value->description->content ?>
                </div>
              </div>
            <?php endforeach ?>
            </div>
            
          </div>
          <!-- end list key people -->

          <div class="clear"></div>
        </div>
        <!-- End middle keyPeople -->

        <div class="clear"></div>
      </div>
    </div>
    <!-- End list key people -->
  </section>

  <section class="blocks_project_installation">
    <div class="prelatife container">
      <div class="insides content-text">
        <h4>Prestigious Project Installation</h4>
        <div class="clear"></div>

        <div class="lists_projectInstallation">
          <div class="row default">
            <?php if ($model_project): ?>
              <ul id="light-gallery" class="gallery list-inline">
              <?php foreach ($model_project as $key => $value): ?>
                <li class="list-inline-item" data-sub-html="<h4><?php echo $value->title; ?></h4><?php echo $value->content; ?>" data-src="<?php echo Yii::app()->baseUrl; ?>/images/project/<?php echo $value->image ?>">
                <div class="col-12">
                  <div class="items">
                  <a href="<?php echo Yii::app()->baseUrl; ?>/images/project/<?php echo $value->image ?>">
                    <div class="pict"><img src="<?php echo Yii::app()->baseUrl; ?>/images/project/<?php echo $value->image ?>" alt="" class="img img-fluid"></div>
                    <div class="info">
                      <p><b><?php echo ucwords($value->title) ?></b></p>
                      <div class="clear"></div>
                    </div>
                    </a>
                  </div>
                </div>
                </li>
              <?php endforeach ?>
              </ul>
            <?php endif ?>

          </div>
        </div>
        <!-- End list default project Installation -->

        <div class="clear"></div>
      </div>
    </div>
  </section>

  <div class="clear"></div>
</div>
  <!-- end container -->

<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.1/css/lightgallery.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.1/js/lightgallery.min.js"></script>
<script>
   $(document).ready(function() {
    $("#light-gallery").lightGallery();
  });
</script>