<div class="blocks_subpage_banner news mah546">
  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900, 550, '/images/static/'.$this->setting['illustration_news_pict'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block full_pict">
  <div class="insides text-center">
    <h3 class="sub_title_p">CORPORATE NEWS</h3>
    <div class="clear"></div>
  </div>
</div>
<style type="text/css">
  img.full_pict{
    width: 100%;
  }
  .outers_back-header{
    position: relative;
  }
  .blocks_subpage_banner.news{
    height: auto; min-height: inherit;
  }
  .blocks_subpage_banner.news .insides{
    position: absolute; z-index: 50;
    left: 0px; width: 100%;
    top: 50%; height: 100%;
  }
  .blocks_subpage_banner h3.sub_title_p{
    padding-top: 0;
  }
</style>

<div class="clear"></div>
  <div class="subpage static_about">
  <div class="prelatife container">
    <div class="tops_contStatic">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <h2 class="titles">NEWS</h2>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="outs_breadcrumb text-right float-right">
            <ol class="breadcrumb">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li class="active">NEWS</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="prelatife container">
    
    <div class="content-text middles_contbottom">
      
      <div class="row default">
        <div class="col-md-9">
          <div class="details_news">
            <div class="row default">
              <div class="col-md-12">
                <div class="pict">
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(980,450, '/images/blog/'.$detail->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                </div>
              </div>
              <div class="col-md-12">
                <div class="info padding-top-30">
                  <h3 class="titles"><?php echo $detail->description->title ?></h3>
                  <span class="dates"><?php echo date('d M Y', strtotime($detail->date_input)) ?></span>
                  <div class="clear"></div>
                  <?php echo $detail->description->content ?>
                  <div class="clear"></div>
                </div>
              </div>
            </div>

          </div>
          <!-- End detail news -->
        </div>
        <div class="col-md-3">
           <div class="block_archives_news">
        <div class="top">
          <h3>OTHER NEWS</h3>
        </div>
        <div class="clear height-45"></div>

        <div class="lists_news_default">
          <div class="row">
          <?php foreach ($blogOther as $key => $value): ?>
            <div class="col-md-12 col-sm-6">
              <div class="items">
                <div class="pict"><a href="<?php echo CHtml::normalizeUrl(array('/home/newsDetail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(403,185, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a></div>
                <div class="info">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/newsDetail', 'id'=>$value->id)); ?>"><h3 class="titles"><?php echo $value->description->title ?></h3></a>
                  <!-- <span class="dates"><?php // echo date('d M Y', strtotime($value->date_input)) ?></span>
                  <div class="clear"></div> -->
                  <!-- <p><?php // echo substr(strip_tags($value->description->content), 0, 200) ?>....</p> -->
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/newsDetail', 'id'=>$value->id)); ?>" class="btn btn-link vw_more_news"><img src="<?php echo $this->assetBaseurl ?>backs_icon_blueNews.png" alt="" class="d-inline">Read More</a>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
          <?php endforeach ?>
          </div>
        </div>
        <div class="clear"></div>
      </div>
        </div>
      </div>
      <!-- End details middles -->

     

      <div class="clear height-50"></div>
      <div class="clear height-40"></div>
      <div class="height-10"></div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>
  <!-- end container -->
