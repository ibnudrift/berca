<div class="blocks_subpage_banner about mah546">
  <div class="insides text-center">
    <h3 class="sub_title_p"><img src="<?php echo $this->assetBaseurl ?>txt-our_journey-berca.png" alt=""></h3>
    <div class="clear"></div>
  </div>
</div>

<div class="clear"></div>
  <div class="subpage static_about">
  <div class="prelatife container">
    <div class="tops_contStatic">
      <div class="row">
        <div class="col-md-6">
          <h2 class="titles">OUR JOURNEY</h2>
        </div>
        <div class="col-md-6">
          <div class="outs_breadcrumb text-right">
            <ol class="breadcrumb">
              <li><a href="#">HOME</a></li>
              <li class="active">OUR JOURNEY</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="prelatife container">
    <div class="clear height-25"></div>
    <div class="content-text middles_contbottom">
      <div class="row">
        <div class="col-md-6">
          <div class="lefts_cont">
            <h3>Berca has became the trusted name for decades in the trading industry in Indonesia. Now, Berca is proud to introduce you, Carrier and Toshiba air conditioning as part of the business network.</h3>
            <p>As we move into a more environmentally conscious and efficient era which requires us to be socially responsible and yet concious towards energy savings, Berca's move with bringing Carrier and Toshiba is a strategic and monumental movement that will act as a new trail to be followed by both lifestyle and industry trend in Indonesia. As Carrier and Toshiba have invented perfect solutions and both brands are unargueable the best brands in the world, trusting our brands would surely make the world a healthier and better place to live in for now and the next generation.</p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="picts_about">
            <img src="<?php echo $this->assetBaseurl ?>pict-about-1.jpg" alt="" class="img-responsive">
          </div>
        </div>
      </div>

      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
  <div class="clear"></div>

  <div class="blocks_middle_content history_block prelatife">
    <div class="container content-text">
    <div class="row row-eq-height">
      <div class="col-md-6 back-grey">
        <div class="inside maw475">
          <h3>Berca&rsquo;s history time line</h3>

          <p>We are the leading company for food waste disposal based in Korea, proud to introduce to you, SmartCara our revolutionary product for food waste disposal.</p>

          <p>As we move into a more environmentally conscious era which requires us to be socially responsible towards the rest of society and future generations. the act of disposing waste in an effective manner that requires minimum waste land is of the utmost importance. after ten years of research and development, we have invented the perfect solution, a food waste disposal unit which would make the world a healthier and better place to live in for now and the next generation.</p>

          <div class="clear"></div>
        </div>
      </div>
      <div class="col-md-6 back-cream">
        <div class="inside timesline_blockhistory">
          <ul class="list-unstyled">
            <li class="dropdown">
              <a href="javascript:;"><span class="year">1993</span> <i class="lines_1"></i> <span class="month">APR</span> <div class="info">First partnership with Carrier</div></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">MAY</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a></li>
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">AUG</span> <div class="info">Carrier to introduce the HVAC solution to the public</div></a></li>
              </ul>
            </li>
            <li>
              <a href="javascript:;"><span class="year">1998</span> <i class="lines_1"></i> <span class="month">JAN</span> <div class="info">4 Network offices opening on Java, covering Surabaya, Semarang, Yogyakarta, Bandung and Bali.</div></a>
            </li>
            <li class="dropdown">
              <a href="javascript:;"><span class="year">2005</span> <i class="lines_1"></i> <span class="month">MAY</span> <div class="info">Carrier launched  AquaEdge&reg; 23XRV with Greenspeed Technology</div></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">MAY</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a></li>
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">AUG</span> <div class="info">Carrier to introduce the HVAC solution to the public</div></a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="javascript:;"><span class="year">2008</span> <i class="lines_1"></i> <span class="month">JAN</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">APR</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a></li>
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">SEP</span> <div class="info">Carrier to introduce the HVAC solution to the public</div></a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="javascript:;"><span class="year">2017</span> <i class="lines_1"></i> <span class="month">JAN</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">FEB</span> <div class="info">Initial warehouse & headquarter establishment at Jakarta</div></a></li>
                <li><a href="javascript:;"><i class="lines_2"></i> <span class="month">APR</span> <div class="info">Carrier to introduce the HVAC solution to the public</div></a></li>
              </ul>
            </li>
          </ul>
          <div class="clear"></div>
        </div>
      </div>
    </div>
    </div>
    <div class="clear"></div>
  </div>

  <div class="block_about_vision">
    <div class="prelatife container">
      <div class="clear height-50"></div>
      <div class="clear height-50"></div>
      <div class="clear height-40"></div>

      <div class="cont_vision text-center">
        <div class="row default">
          <div class="col-md-6 col-sm-6 bdr_right">
            <div class="texts">
              <h5>OUR VISION</h5>
              <div class="clear"></div>
              <p>Delivering powerful solutions to both retail and corporate market through world&#39;s best brand and product, and thus became the standard for the needs of high-performance and reliable air cooling / refrigeration in Indonesia.</p>
              <div class="clear"></div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="texts">
              <h5>OUR MISSION</h5>
              <div class="clear"></div>
              <p>Introducing a broad range of applications in air conditioning and refrigeration technology, backed with full support of dedicated field professionals and product quality assurance.</p>
              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="clear"></div>
    </div>
  </div>

  <section class="blocks_managements_about_outer">
    <div class="outer_block_managements">
      <div class="prelatife container">
        <div class="insides content-text">
          <div class="clear height-50"></div>
          <div class="height-10"></div>
          <div class="row">
            <div class="col-md-6">
              <h4>Our Management</h4>
              <div class="clear"></div>
              <h6>Meet Berca's management team. The management's purpose is to guide the company to achieve our vision, mission and goals, as well as being responsible run the company with excellent operation procedure.</h6>
              <p>The management operates under the chairmanship of the Chief Executive Officer and dedicated to be responsible and to make sure the deployment of the corporate' culture, strategy and policies. The management will work hard to reach top-tier financial performance and make a good brand presence in Indonesia.</p>

            </div>
            <div class="col-md-6"></div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
    <div class="outers_block_keyPeople">
      <div class="prelatife container">

        <div class="ins_middle_keyPeople prelatife">
          <h4>Key People</h4>

          <div class="clear"></div>
          <div class="lists_defaults_keyPeople">
            <div class="row default">
            <?php for ($i=1; $i < 5; $i++) { ?>
              <div class="col-md-3">
                <div class="items">
                  <div class="top padding-bottom-20">
                    <div class="picture fleft"><img src="<?php echo $this->assetBaseurl ?>ex_pict_keypeople.jpg" alt="" class="img-responsive"></div>
                    <p><b>Alexander Jameson</b><br>Director</p>
                    <div class="clear"></div>
                  </div>
                  <p>As we move into a more environmentally conscious era which requires us to be socially responsible towards the rest of society and future generations. the act of disposing waste in an effective manner that requires minimum waste land is of the utmost importance. after ten years of research and development, we have invented the perfect solution, a food waste disposal unit which would make the world a healthier and better place to live in for now and the next generation.</p>
                </div>
              </div>
              <?php } ?>
            </div>
            
          </div>
          <!-- end list key people -->

          <div class="clear"></div>
        </div>
        <!-- End middle keyPeople -->

        <div class="clear"></div>
      </div>
    </div>
  </section>

  <section class="blocks_project_installation">
    <div class="prelatife container">
      <div class="insides content-text">
        <h4>Prestigious Project Installation</h4>
        <div class="clear"></div>

        <div class="lists_projectInstallation">
          <div class="row default">
            <?php for ($i=1; $i < 9; $i++) { ?>
            <div class="col-md-3">
              <div class="items">
                <div class="pict"><img src="<?php echo $this->assetBaseurl ?>ex_installation.jpg" alt="" class="img-responsive"></div>
                <div class="info">
                  <p><b>Project Or Client Name</b><br>
                    Project Location</p>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
            <?php } ?>

          </div>
        </div>
        <!-- End list default project Installation -->

        <div class="clear"></div>
      </div>
    </div>
  </section>

  <div class="clear"></div>
</div>
  <!-- end container -->
