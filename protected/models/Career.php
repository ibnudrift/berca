<?php

/**
 * This is the model class for table "career".
 *
 * The followings are the available columns in table 'career':
 * @property integer $id
 * @property string $job_title
 * @property string $experience
 * @property string $work_location
 * @property string $description
 * @property string $requirement
 * @property integer $status
 */
class Career extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'career';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('job_title, experience', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('job_title, experience, work_location', 'length', 'max'=>225),
			// The following rule is used by search().
			array('work_location, description, requirement, status','safe'),
			// @todo Please remove those attributes that should not be searched.
			array('id, job_title, experience, work_location, description, requirement, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'job_title' => 'Job Title',
			'experience' => 'Experience',
			'work_location' => 'Work Location',
			'description' => 'Description',
			'requirement' => 'Requirement',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('job_title',$this->job_title,true);
		$criteria->compare('experience',$this->experience,true);
		$criteria->compare('work_location',$this->work_location,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('requirement',$this->requirement,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Career the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
